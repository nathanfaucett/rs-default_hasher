#![feature(sip_hash_13)]
#![no_std]


extern crate rand;
#[cfg(feature = "use_std")]
#[macro_use]
extern crate std;


mod default_hasher;
mod random_state;


pub use self::default_hasher::DefaultHasher;
pub use self::random_state::RandomState;
