#[allow(deprecated)]
use core::hash::{Hasher, SipHasher13};

use rand::{self, Rng};


#[allow(deprecated)]
pub struct DefaultHasher<T = SipHasher13>
    where T: Hasher,
{
    hasher: T,
}

impl<T> Default for DefaultHasher<T>
    where T: Default + Hasher,
{
    #[inline(always)]
    fn default() -> Self {
        Self::new_with_hasher(T::default())
    }
}

impl<T> DefaultHasher<T>
    where T: Hasher,
{
    #[inline(always)]
    pub fn new_with_hasher(hasher: T) -> Self {
        DefaultHasher {
            hasher: hasher,
        }
    }
}

#[allow(deprecated)]
impl DefaultHasher<SipHasher13> {

    #[cfg(not(feature = "use_std"))]
    #[inline(always)]
    pub fn new() -> Self {
        Self::from_rng(&mut rand::weak_rng())
    }

    #[cfg(feature = "use_std")]
    #[inline]
    pub fn new() -> Self {
        use std::cell::Cell;

        thread_local!(static KEYS: Cell<(u64, u64)> = {
            let r = rand::OsRng::new();
            let mut r = r.expect("failed to create an OS RNG");
            Cell::new((r.gen(), r.gen()))
        });

        KEYS.with(|keys| {
            let (k0, k1) = keys.get();
            keys.set((k0.wrapping_add(1), k1));
            Self::new_with_hasher(SipHasher13::new_with_keys(k0, k1))
        })
    }

    #[inline(always)]
    pub fn from_rng<R>(rng: &mut R) -> Self
        where R: Rng,
    {
        Self::new_with_hasher(SipHasher13::new_with_keys(rng.next_u64(), rng.next_u64()))
    }
}

impl<T> Hasher for DefaultHasher<T>
    where T: Hasher,
{
    #[inline(always)]
    fn write(&mut self, msg: &[u8]) {
        self.hasher.write(msg)
    }
    #[inline(always)]
    fn finish(&self) -> u64 {
        self.hasher.finish()
    }
}
