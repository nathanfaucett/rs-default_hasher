#[allow(deprecated)]
use core::hash::{BuildHasher, SipHasher13};

use rand::{self, Rng};

use super::DefaultHasher;


#[derive(Clone)]
pub struct RandomState {
    k0: u64,
    k1: u64,
}

impl RandomState {

    #[cfg(not(feature = "use_std"))]
    #[inline(always)]
    pub fn new() -> Self {
        Self::from_rng(&mut rand::weak_rng())
    }

    #[cfg(feature = "use_std")]
    #[inline]
    pub fn new() -> Self {
        use std::cell::Cell;

        thread_local!(static KEYS: Cell<(u64, u64)> = {
            let r = rand::OsRng::new();
            let mut r = r.expect("failed to create an OS RNG");
            Cell::new((r.gen(), r.gen()))
        });

        KEYS.with(|keys| {
            let (k0, k1) = keys.get();
            keys.set((k0.wrapping_add(1), k1));
            Self::new_with_keys(k0, k1)
        })
    }

    #[inline(always)]
    pub fn from_rng<R>(rng: &mut R) -> Self
        where R: Rng,
    {
        Self::new_with_keys(rng.next_u64(), rng.next_u64())
    }

    #[inline(always)]
    pub fn new_with_keys(k0: u64, k1: u64) -> Self {
        RandomState {
            k0: k0,
            k1: k1,
        }
    }
}

impl Default for RandomState {
    #[inline(always)]
    fn default() -> Self {
        Self::new()
    }
}

#[allow(deprecated)]
impl BuildHasher for RandomState {
    type Hasher = DefaultHasher<SipHasher13>;

    #[inline(always)]
    fn build_hasher(&self) -> Self::Hasher {
        DefaultHasher::new_with_hasher(SipHasher13::new_with_keys(self.k0, self.k1))
    }
}


#[cfg(test)]
mod test {
    use super::*;

    use core::hash::{Hash, Hasher};


    fn hash<T, S>(value: T, builder: S) -> u64
        where T: Hash,
              S: BuildHasher,
    {
        let mut hasher = builder.build_hasher();
        value.hash(&mut hasher);
        hasher.finish()
    }

    #[test]
    fn test_hash() {
        assert_eq!(hash(0, RandomState { k0: 0, k1: 1 }), 13030642244368236042);
    }

    #[test]
    fn test_random_hash() {
        let _ = hash(0, RandomState::default());
    }
}
