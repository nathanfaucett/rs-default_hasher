default_hasher
=====

default hasher

```rust
extern crate default_hasher;


use default_hasher::RandomState;
use std::hash::{Hash, Hasher, BuildHasher};


fn main() {
    let mut hasher = RandomState::new().build_hasher();
    10.hash(&mut hasher);
    println!("{:?}", hasher.finish());
}
```
